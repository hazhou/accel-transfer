// A mimic class of xAOD::UncalibratedMeasurement

#ifndef UNCALIBRATED_MEASUREMENT_H
#define UNCALIBRATED_MEASUREMENT_H

#include <array>

class UncalibratedMeasurement
{
public:
    // Constructors and destructor
    UncalibratedMeasurement() = default;
    UncalibratedMeasurement(std::array<float, 3> localPosition, std::array<float, 9> localCovariance, unsigned int identifierHash);
    virtual ~UncalibratedMeasurement() = default;

    // Data members
    std::array<float, 3> m_localPosition;
    std::array<float, 9> m_localCovariance;
    unsigned int m_identifierHash;

    // Getter
    std::array<float, 3> getLocalPosition();
    std::array<float, 9> getLocalCovariance();
    unsigned int getIdentifierHash();

    // Setter
    void setLocalPosition(std::array<float, 3> localPosition);
    void setLocalCovariance(std::array<float, 9> localCovariance);
    void setIdentifierHash(unsigned int identifierHash);
};

#endif