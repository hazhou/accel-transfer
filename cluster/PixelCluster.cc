#include "PixelCluster.hh"
#include <array>
#include <vector>

PixelCluster::PixelCluster(std::array<float, 3> globalPosition,
                           int channelsInPhi,
                           int channelsInEta,
                           float omegaX,
                           float omegaY,
                           std::vector<int> totList,
                           int totalToT,
                           std::vector<float> chargeList,
                           float totalCharge,
                           float energyLoss,
                           bool isSplit,
                           float splitProbability1,
                           float splitProbability2,
                           int lvl1a,
                           std::array<float, 3> localPosition,
                           std::array<float, 9> localCovariance,
                           unsigned int identifierHash)
    : UncalibratedMeasurement(localPosition, localCovariance, identifierHash),
      m_globalPosition(globalPosition),
      m_channelsInPhi(channelsInPhi),
      m_channelsInEta(channelsInEta),
      m_omegaX(omegaX),
      m_omegaY(omegaY),
      m_totList(totList),
      m_totalToT(totalToT),
      m_chargeList(chargeList),
      m_totalCharge(totalCharge),
      m_energyLoss(energyLoss),
      m_isSplit(isSplit),
      m_splitProbability1(splitProbability1),
      m_splitProbability2(splitProbability2),
      m_lvl1a(lvl1a)
{
}

std::array<float, 3> PixelCluster::getGlobalPosition()
{
    return m_globalPosition;
}

int PixelCluster::getChannelsInPhi()
{
    return m_channelsInPhi;
}

int PixelCluster::getChannelsInEta()
{
    return m_channelsInEta;
}

float PixelCluster::getOmegaX()
{
    return m_omegaX;
}

float PixelCluster::getOmegaY()
{
    return m_omegaY;
}

std::vector<int> PixelCluster::getTotList()
{
    return m_totList;
}

int PixelCluster::getTotalToT()
{
    return m_totalToT;
}

std::vector<float> PixelCluster::getChargeList()
{
    return m_chargeList;
}

float PixelCluster::getTotalCharge()
{
    return m_totalCharge;
}

float PixelCluster::getEnergyLoss()
{
    return m_energyLoss;
}

bool PixelCluster::getIsSplit()
{
    return m_isSplit;
}

float PixelCluster::getSplitProbability1()
{
    return m_splitProbability1;
}

float PixelCluster::getSplitProbability2()
{
    return m_splitProbability2;
}

int PixelCluster::getLvl1a()
{
    return m_lvl1a;
}

void PixelCluster::setGlobalPosition(std::array<float, 3> globalPosition)
{
    m_globalPosition = globalPosition;
}

void PixelCluster::setChannelsInPhi(int channelsInPhi)
{
    m_channelsInPhi = channelsInPhi;
}

void PixelCluster::setChannelsInEta(int channelsInEta)
{
    m_channelsInEta = channelsInEta;
}

void PixelCluster::setOmegaX(float omegaX)
{
    m_omegaX = omegaX;
}

void PixelCluster::setOmegaY(float omegaY)
{
    m_omegaY = omegaY;
}

void PixelCluster::setTotList(std::vector<int> totList)
{
    m_totList = totList;
}

void PixelCluster::setChargeList(std::vector<float> chargeList)
{
    m_chargeList = chargeList;
}

void PixelCluster::setTotalCharge(float totalCharge)
{
    m_totalCharge = totalCharge;
}

void PixelCluster::setEnergyLoss(float energyLoss)
{
    m_energyLoss = energyLoss;
}

void PixelCluster::setIsSplit(bool isSplit)
{
    m_isSplit = isSplit;
}

void PixelCluster::setSplitProbability1(float splitProbability1)
{
    m_splitProbability1 = splitProbability1;
}

void PixelCluster::setSplitProbability2(float splitProbability2)
{
    m_splitProbability2 = splitProbability2;
}

void PixelCluster::setLvl1a(int lvl1a)
{
    m_lvl1a = lvl1a;
}