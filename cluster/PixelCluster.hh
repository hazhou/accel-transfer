#ifndef PIXEL_CLUSTER_HH
#define PIXEL_CLUSTER_HH
// A AOD-like cluster class
#include <array>
#include <vector>
#include "UncalibratedMeasurement.hh"

class PixelCluster : public UncalibratedMeasurement
{
public:
    // Default constructor and destructors
    PixelCluster(){};
    PixelCluster(std::array<float, 3> globalPosition,
                 int channelsInPhi,
                 int channelsInEta,
                 float omegaX,
                 float omegaY,
                 std::vector<int> totList,
                 int totalToT,
                 std::vector<float> chargeList,
                 float totalCharge,
                 float energyLoss,
                 bool isSplit,
                 float splitProbability1,
                 float splitProbability2,
                 int lvl1a,
                 std::array<float,3> localPosition,
                 std::array<float,9> localCovariance,
                 unsigned int identifierHash);
    ~PixelCluster(){};

    // Data members
    std::array<float, 3> m_globalPosition;
    int m_channelsInPhi;
    int m_channelsInEta;
    // the charge balance between the first and last rows and colums, respecitvely,
    // building the cluster, and are numbers between 0 and 1
    float m_omegaX;
    float m_omegaY;
    std::vector<int> m_totList;
    // sum of the ToTs of the cahnnels building the cluster
    int m_totalToT;
    std::vector<float> m_chargeList;
    float m_totalCharge;
    float m_energyLoss;
    // if the cluster is split or not
    bool m_isSplit;
    // splitting probabilities fo the cluster
    float m_splitProbability1;
    float m_splitProbability2;
    // LVL1 accept
    int m_lvl1a;

    // Getter
    std::array<float, 3> getGlobalPosition();
    int getChannelsInPhi();
    int getChannelsInEta();
    float getOmegaX();
    float getOmegaY();
    std::vector<int> getTotList();
    int getTotalToT();
    std::vector<float> getChargeList();
    float getTotalCharge();
    float getEnergyLoss();
    bool getIsSplit();
    float getSplitProbability1();
    float getSplitProbability2();
    int getLvl1a();

    // Setter
    void setGlobalPosition(std::array<float, 3> globalPosition);
    void setChannelsInPhi(int channelsInPhi);
    void setChannelsInEta(int channelsInEta);
    void setOmegaX(float omegaX);
    void setOmegaY(float omegaY);
    void setTotList(std::vector<int> totList);
    void setTotalToT(int totalToT);
    void setChargeList(std::vector<float> chargeList);
    void setTotalCharge(float totalCharge);
    void setEnergyLoss(float energyLoss);
    void setIsSplit(bool isSplit);
    void setSplitProbability1(float splitProbability1);
    void setSplitProbability2(float splitProbability2);
    void setLvl1a(int lvl1a);
};

#endif