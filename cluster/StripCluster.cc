#include "StripCluster.hh"
#include <array>

StripCluster::StripCluster(std::array<float, 3> globalPosition,
                           int channelsInPhi,
                           int hitsInThirdTimeBin,
                           std::array<float, 3> localPosition,
                           std::array<float, 9> localCovariance,
                           unsigned int identifierHash)
    : UncalibratedMeasurement(localPosition, localCovariance, identifierHash),
      m_globalPosition(globalPosition),
      m_channelsInPhi(channelsInPhi),
      m_hitsInThirdTimeBin(hitsInThirdTimeBin)

{
}

std::array<float, 3> StripCluster::getGlobalPosition()
{
    return m_globalPosition;
}

int StripCluster::getChannelsInPhi()
{
    return m_channelsInPhi;
}

int StripCluster::getHitsInThirdTimeBin()
{
    return m_hitsInThirdTimeBin;
}

void StripCluster::setGlobalPosition(std::array<float, 3> globalPosition)
{
    m_globalPosition = globalPosition;
}

void StripCluster::setChannelsInPhi(int channelsInPhi)
{
    m_channelsInPhi = channelsInPhi;
}
