#include <xrt/xrt_kernel.h>
#include <xrt/xrt_bo.h>

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include "UncalibratedMeasurement.hh"
#include "StripCluster.hh"


int main()
{
    bool debug = false;

    // Find device and load kernel
    unsigned int device_index = 0;
    auto device = xrt::device(device_index);
    auto uuid = device.load_xclbin("transfer.xclbin");

    unsigned int input_size = sizeof(int)*2;
    // unsigned int input_size = sizeof(int)*2;
    unsigned int pos_size = sizeof(float)*6;

    // Identify kernel and create instance
    auto krnl = xrt::kernel(device, uuid, "transfer");

    // Allocate buffer in global memory
    auto bo_input = xrt::bo(device, input_size, krnl.group_id(0));
    auto bo_output = xrt::bo(device, input_size, krnl.group_id(1));
    auto bo_inpos = xrt::bo(device, pos_size, krnl.group_id(2));
    auto bo_outpos = xrt::bo(device, pos_size, krnl.group_id(3));

    // Map the contents of the buffer object into host memory
    // This method maps the host-side buffer backing pointer to a user pointer
    auto bo_input_map = bo_input.map<int *>();
    auto bo_output_map = bo_output.map<int *>();
    auto bo_inpos_map = bo_inpos.map<float *>();
    auto bo_outpos_map = bo_outpos.map<float *>();

    // Declare cluster class
    std::array<float,3> localPosition={1.1,2.2,3.3};
    std::array<float,3> globalPosition={4.4,5.5,6.6};
    std::array<float,9> localCovariance = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};
    unsigned int identifierHash = 1234567890;

    StripCluster sc;
    sc.setLocalPosition(localPosition);
    sc.setLocalCovariance(localCovariance);
    sc.setIdentifierHash(identifierHash);
    sc.setGlobalPosition(globalPosition);
    sc.setChannelsInPhi(996);

    std::cout<<"[Host] Channels in phi is "<<sc.getChannelsInPhi()<<std::endl;
    std::cout<<"[Host] Global position is: ("<<sc.getGlobalPosition().at(0)<<", "<<sc.getGlobalPosition().at(1)<<", "<<sc.getGlobalPosition().at(2)<<")"<<std::endl;
    std::cout<<"[Host] Local position is: ("<<sc.getLocalPosition().at(0)<<", "<<sc.getLocalPosition().at(1)<<", "<<sc.getLocalPosition().at(2)<<")"<<std::endl;
    std::cout << "[Host] Local Covariance is: [";
    for (auto cov : sc.getLocalCovariance())
    {
        std::cout << cov << " ";
    }
    std::cout << "]\n";
    std::cout << "[Host] Identifier hash is: " << sc.getIdentifierHash() << std::endl;
    
    bo_input_map[0] = sc.getChannelsInPhi();
    // bo_input_map[1] = cluster.getChannelsInEta();
    for(int i=0;i<sc.getGlobalPosition().size()+sc.getLocalPosition().size();i++)
    {
        // Global position goes first
        if(i<sc.getGlobalPosition().size())
        {
            bo_inpos_map[i]=sc.getGlobalPosition().at(i);
        }

        // Local position goes second
        else
        {
            bo_inpos_map[i]=sc.getLocalPosition().at(i-sc.getGlobalPosition().size());
        }
        std::cout<<bo_inpos_map[i]<<std::endl;
    }
  

    // Synchronize buffer content with device side
    bo_input.sync(XCL_BO_SYNC_BO_TO_DEVICE);
    bo_inpos.sync(XCL_BO_SYNC_BO_TO_DEVICE);

    // Run acclerator card tracking
    auto run = krnl(bo_input, bo_output, bo_inpos, bo_outpos);
    run.wait();

    bo_output.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
    bo_outpos.sync(XCL_BO_SYNC_BO_FROM_DEVICE);

    // Print out results
    std::cout<<"[Device Buffer object] Channels in phi is "<<bo_output_map[0]<<std::endl;
    // std::cout<<"[Device Buffer object] Channels in eta is "<<bo_output_map[1]<<std::endl;
    std::cout<<"[Device Buffer object] Global position: ("<<bo_outpos_map[0]<<", "<<bo_outpos_map[1]<<", "<<bo_outpos_map[2]<<")"<<std::endl;
    std::cout<<"[Device Buffer object] Local position: ("<<bo_outpos_map[3]<<", "<<bo_outpos_map[4]<<", "<<bo_outpos_map[5]<<")"<<std::endl;
    

    return 0;
}