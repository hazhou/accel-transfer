# Makefile to build host and kernel for U250, modified from Vitis example
# zhaoyuan.cui@cern.ch
# Physics department, University of Ariona

TARGET := sw_emu
# TARGET := hw_emu
#TARGET := hw
KNL_NAME := transfer
PLATFORM := xilinx_u250_gen3x16_xdma_4_1_202210_1
SRCDIR := $(shell git rev-parse --show-toplevel)/src
BUILD_DIR := $(TARGET)

### My Modifications
#### SW_EMU ######
run_sw_emu: build_sw_emu
	cd sw_emu && XCL_EMULATION_MODE=sw_emu ./host_openCL

build_sw_emu: host_sw_emu emconfig_sw_emu xclbin_sw_emu

host_sw_emu: sw_emu/host_openCL
sw_emu/host_openCL:
	@echo "Making sw_emu host"
	mkdir -p ./sw_emu
	g++ --std=c++1y -I$(SRCDIR)/../cluster -I$XILINX_XRT/include -L$XILINX_XRT/lib -lOpenCL -lrt -pthread $(SRCDIR)/host_openCL.cc -o sw_emu/host_openCL

emconfig_sw_emu: sw_emu/emconfig.json
sw_emu/emconfig.json:
	@echo "Generating emulation file"
	emconfigutil -f $(PLATFORM) --od sw_emu --nd 1

xo_sw_emu: sw_emu/$(KNL_NAME).xo
sw_emu/$(KNL_NAME).xo:
	v++ -c -t sw_emu --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg -k $(KNL_NAME) -I$(SRCDIR) $(SRCDIR)/transfer.cpp -o sw_emu/$(KNL_NAME).xo

xclbin_sw_emu: sw_emu/$(KNL_NAME).xclbin
sw_emu/$(KNL_NAME).xclbin: sw_emu/$(KNL_NAME).xo
	v++ -l -t sw_emu --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg sw_emu/$(KNL_NAME).xo -o sw_emu/$(KNL_NAME).xclbin
###############
############ HW_EMU ################
run_hw_emu: build_hw_emu
	cd hw_emu && XCL_EMULATION_MODE=hw_emu ./host_openCL

build_hw_emu: host_hw_emu emconfig_hw_emu xclbin_hw_emu

host_hw_emu: hw_emu/host_openCL
hw_emu/host_openCL:
	@echo "Making hw_emu host"
	mkdir -p ./hw_emu
	g++ --std=c++1y -I$(SRCDIR)/../cluster -I$XILINX_XRT/include -L$XILINX_XRT/lib -lOpenCL -lrt -pthread $(SRCDIR)/host_openCL.cc -o hw_emu/host_openCL

emconfig_hw_emu: hw_emu/emconfig.json
hw_emu/emconfig.json:
	@echo "Generating emulation file"
	emconfigutil -f $(PLATFORM) --od hw_emu --nd 1

xo_hw_emu: hw_emu/$(KNL_NAME).xo
hw_emu/$(KNL_NAME).xo:
	v++ -c -t hw_emu --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg -k $(KNL_NAME) -I$(SRCDIR) $(SRCDIR)/transfer.cpp -o hw_emu/$(KNL_NAME).xo

xclbin_hw_emu: hw_emu/$(KNL_NAME).xclbin
hw_emu/$(KNL_NAME).xclbin: hw_emu/$(KNL_NAME).xo
	v++ -l -t hw_emu --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg hw_emu/$(KNL_NAME).xo -o hw_emu/$(KNL_NAME).xclbin
##################################

############ HW ################
run_hw: build_hw
	@echo "Running on hardware"
	cd hw && ./host_openCL

build_hw: host_hw emconfig_hw xclbin_hw

host_hw: hw/host_openCL
hw/host_openCL:
	@echo "Making hw host"
	mkdir -p ./hw
	g++ --std=c++1y -I$(SRCDIR)/../cluster -I$XILINX_XRT/include -L$XILINX_XRT/lib -lOpenCL -lrt -pthread $(SRCDIR)/host_openCL.cc -o hw/host_openCL

emconfig_hw: hw/emconfig.json
hw/emconfig.json:
	@echo "Generating emulation file"
	emconfigutil -f $(PLATFORM) --od hw --nd 1

xo_hw: hw/$(KNL_NAME).xo
hw/$(KNL_NAME).xo:
	v++ -c -t hw --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg -k $(KNL_NAME) -I$(SRCDIR) $(SRCDIR)/transfer.cpp -o hw/$(KNL_NAME).xo

xclbin_hw: hw/$(KNL_NAME).xclbin
hw/$(KNL_NAME).xclbin: hw/$(KNL_NAME).xo
	v++ -l -t hw --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg hw/$(KNL_NAME).xo -o hw/$(KNL_NAME).xclbin
##################################

# run: build 
# ifeq ($(TARGET),hw)
# 	@echo "Running on hardware"
# 	cd $(BUILD_DIR) && ./host_openCL
# else
# 	cd $(BUILD_DIR) && XCL_EMULATION_MODE=$(TARGET) ./host_openCL

# endif	

# build: host emconfig xclbin

# host: $(BUILD_DIR)/host_openCL
# $(BUILD_DIR)/host_openCL:
# 	@echo "Making host"
# 	mkdir -p ./$(TARGET)
# 	g++ --std=c++1y -I$(SRCDIR)/../cluster -I$XILINX_XRT/include -L$XILINX_XRT/lib -lOpenCL -lrt -pthread $(SRCDIR)/host_openCL.cc -o $(BUILD_DIR)/host_openCL

# emconfig: $(BUILD_DIR)/emconfig.json
# $(BUILD_DIR)/emconfig.json:
# 	@echo "Generating emulation file"
# 	emconfigutil -f $(PLATFORM) --od $(BUILD_DIR) --nd 1 

# xo: $(BUILD_DIR)/$(KNL_NAME).xo
# $(BUILD_DIR)/$(KNL_NAME).xo:
# 	v++ -c -t $(TARGET) --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg -k $(KNL_NAME) -I$(SRCDIR) $(SRCDIR)/transfer.cpp -o $(BUILD_DIR)/$(KNL_NAME).xo

# xclbin: $(BUILD_DIR)/$(KNL_NAME).xclbin
# $(BUILD_DIR)/$(KNL_NAME).xclbin: $(BUILD_DIR)/$(KNL_NAME).xo
# 	v++ -l -t ${TARGET} --platform $(PLATFORM) --config $(SRCDIR)/u250.cfg $(BUILD_DIR)/$(KNL_NAME).xo -o $(BUILD_DIR)/$(KNL_NAME).xclbin

clean:
	rm -r ./$(TARGET) *log _x
